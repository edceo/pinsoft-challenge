package com.pinsoft.challenge.enums;

public enum ErrorCode {

    INVALID_OPERATOR, INVALID_EXPRESSION, CANNOT_DIVIDE_BY_ZERO, INVALID_MINEFIELD
}
