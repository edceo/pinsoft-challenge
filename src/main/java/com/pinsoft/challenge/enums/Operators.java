package com.pinsoft.challenge.enums;

import com.pinsoft.challenge.exceptions.ChallengeException;

/**
 * @author Yusuf ONDER
 */

public enum Operators {

    ADD {
        @Override
        public int calculate(int a, int b) {
            return a + b;
        }
    },
    SUBTRACT {
        @Override
        public int calculate(int a, int b) {
            return a - b;
        }
    }, MULTIPLY {
        @Override
        public int calculate(int a, int b) {
            return a * b;
        }
    }, DIVIDE {
        @Override
        public int calculate(int a, int b) {
            if (b == 0) {
                throw new ChallengeException(ErrorCode.CANNOT_DIVIDE_BY_ZERO, "Cannot divide by zero");
            }
            return a / b;
        }
    };

    public static Operators getOperator(String operator) {
        return switch (operator) {
            case "+" -> ADD;
            case "-" -> SUBTRACT;
            case "*" -> MULTIPLY;
            case "/" -> DIVIDE;
            default -> throw new ChallengeException(ErrorCode.INVALID_OPERATOR, "Invalid operation" + operator);
        };
    }

    abstract public int calculate(int a, int b);
}
