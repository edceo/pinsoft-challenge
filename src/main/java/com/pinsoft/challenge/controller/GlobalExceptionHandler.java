package com.pinsoft.challenge.controller;

import com.pinsoft.challenge.enums.ErrorCode;
import com.pinsoft.challenge.exceptions.ChallengeException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ChallengeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorCode handleChallengeException(ChallengeException challengeException) {
        return challengeException.getErrorCode();
    }

}
