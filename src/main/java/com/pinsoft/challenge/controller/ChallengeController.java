package com.pinsoft.challenge.controller;

import com.pinsoft.challenge.manager.ChallengeManager;
import com.pinsoft.challenge.model.request.MinesweeperRequest;
import com.pinsoft.challenge.model.request.RpnRequest;
import com.pinsoft.challenge.model.response.MinesweeperResponse;
import com.pinsoft.challenge.model.response.RpnResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Yusuf ONDER
 */
@RestController
@RequestMapping("/challenge")
@RequiredArgsConstructor
public class ChallengeController {

    private final ChallengeManager challengeManager;

    @PostMapping("/calculate")
    public ResponseEntity<RpnResponse> calculate(@RequestBody RpnRequest rpnExpression) {
        RpnResponse rpnResponse = challengeManager.calculateRpn(rpnExpression);
        return ResponseEntity.ok().body(rpnResponse);
    }

    @PostMapping("/show-hints")
    public ResponseEntity<MinesweeperResponse> showHints(@RequestBody MinesweeperRequest request) {
        MinesweeperResponse minesweeperResponse = challengeManager.showHints(request);
        return ResponseEntity.ok().body(minesweeperResponse);
    }
}
