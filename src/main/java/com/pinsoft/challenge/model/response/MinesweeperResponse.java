package com.pinsoft.challenge.model.response;

/**
 * @author Yusuf ONDER
 */

public record MinesweeperResponse(String[] hints) {
}
