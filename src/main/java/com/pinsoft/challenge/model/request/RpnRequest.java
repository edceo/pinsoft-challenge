package com.pinsoft.challenge.model.request;

/**
 * @author Yusuf ONDER
 */
public record RpnRequest(String expression) {
}
