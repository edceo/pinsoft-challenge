package com.pinsoft.challenge.model.request;

/**
 * @author Yusuf ONDER
 */

public record MinesweeperRequest(String[] square) {
}
