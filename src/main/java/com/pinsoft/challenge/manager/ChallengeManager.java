package com.pinsoft.challenge.manager;

import com.pinsoft.challenge.enums.ErrorCode;
import com.pinsoft.challenge.enums.Operators;
import com.pinsoft.challenge.exceptions.ChallengeException;
import com.pinsoft.challenge.model.request.MinesweeperRequest;
import com.pinsoft.challenge.model.request.RpnRequest;
import com.pinsoft.challenge.model.response.MinesweeperResponse;
import com.pinsoft.challenge.model.response.RpnResponse;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Stack;

/**
 * @author Yusuf ONDER
 */
@Component
public class ChallengeManager {

    public RpnResponse calculateRpn(RpnRequest rpnExpression) {
        Stack<Integer> stack = new Stack<>();
        String[] splittedExpression = rpnExpression.expression().split(" ");
        Arrays.stream(splittedExpression).forEach(expression -> {
            boolean isNumber = expression.chars().allMatch(Character::isDigit);
            if (isNumber) {
                stack.push(Integer.parseInt(expression));
            } else {
                int second = stack.pop();
                int first = stack.pop();
                int addToStack = Operators.getOperator(expression).calculate(first, second);
                stack.push(addToStack);
            }
        });

        if (stack.size() != 1) {
            throw new ChallengeException(ErrorCode.INVALID_EXPRESSION, "Invalid expression");
        }

        return new RpnResponse(stack.pop());
    }

    public MinesweeperResponse showHints(MinesweeperRequest request) {
        String[] square = request.square();
        String[][] calculatedSquare = new String[square.length][square[0].length()];
        String[] result = new String[square.length];
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < square.length; i++) {
            for (int j = 0; j < square[i].length(); j++) {
                if (square[i].charAt(j) == '*') {
                    calculatedSquare[i][j] = "*";
                } else {
                    calculatedSquare[i][j] = countMines(square, i, j);
                }
                builder.append(calculatedSquare[i][j]);
            }
            result[i] = builder.toString();
            builder.setLength(0);
        }
        return new MinesweeperResponse(result);
    }

    private String countMines(String[] square, int i, int j) {
        int count = 0;
        for (int k = i - 1; k <= i + 1; k++) {
            for (int l = j - 1; l <= j + 1; l++) {
                boolean checkRow = k >= 0 && k < square.length;
                boolean checkColumn = l >= 0 && l < square[0].length();
                if (checkRow && checkColumn) {
                    boolean isMines = square[k].charAt(l) == '*';
                    if (isMines) {
                        count++;
                    }
                }
            }
        }
        return String.valueOf(count);
    }
}
