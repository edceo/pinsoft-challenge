package com.pinsoft.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PinsoftApplication {

    public static void main(String[] args) {
        SpringApplication.run(PinsoftApplication.class, args);
    }

}
