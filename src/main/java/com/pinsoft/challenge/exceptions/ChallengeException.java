package com.pinsoft.challenge.exceptions;

import com.pinsoft.challenge.enums.ErrorCode;
import lombok.Getter;

import java.io.Serial;

@Getter
public class ChallengeException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;
    private final ErrorCode errorCode;

    public ChallengeException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
}
