package com.pinsoft.challenge.manager;

import com.pinsoft.challenge.enums.ErrorCode;
import com.pinsoft.challenge.exceptions.ChallengeException;
import com.pinsoft.challenge.model.request.MinesweeperRequest;
import com.pinsoft.challenge.model.request.RpnRequest;
import com.pinsoft.challenge.model.response.RpnResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ChallengeManagerTest {
    @InjectMocks
    private ChallengeManager challengeManager;

    @Test
    @DisplayName("Calculate Rpn Success Case")
    void calculateRpn_Success() {
        RpnRequest rpnRequest = mock(RpnRequest.class);
        when(rpnRequest.expression()).thenReturn("3 5 8 * 7 + *");
        RpnResponse rpnResponse = challengeManager.calculateRpn(rpnRequest);
        assertEquals(141, rpnResponse.result());
        verify(rpnRequest).expression();
    }

    @Test
    @DisplayName("InvalidExpression Case")
    void calculateRpn_InvalidExpression_ThrowInvalidExpression() {
        RpnRequest rpnRequest = mock(RpnRequest.class);
        when(rpnRequest.expression()).thenReturn("3 5 8 * 7 +");
        ChallengeException thrown = assertThrows(ChallengeException.class, () -> challengeManager.calculateRpn(rpnRequest));
        assertEquals(ErrorCode.INVALID_EXPRESSION, thrown.getErrorCode());
        verify(rpnRequest).expression();
    }

    @Test
    @DisplayName("InvalidOperator Case")
    void calculateRpn_InvalidOperator_ThrowInvalidOperator() {
        RpnRequest rpnRequest = mock(RpnRequest.class);
        when(rpnRequest.expression()).thenReturn("3 5 8 * 7 =");
        ChallengeException thrown = assertThrows(ChallengeException.class, () -> challengeManager.calculateRpn(rpnRequest));
        assertEquals(ErrorCode.INVALID_OPERATOR, thrown.getErrorCode());
        verify(rpnRequest).expression();
    }

    @Test
    @DisplayName("DivideByZero Case")
    void calculateRpn_DivideByZero_ThrowCannotDivideByZero() {
        RpnRequest rpnRequest = mock(RpnRequest.class);
        when(rpnRequest.expression()).thenReturn("3 8 0 / 7 + *");
        ChallengeException thrown = assertThrows(ChallengeException.class, () -> challengeManager.calculateRpn(rpnRequest));
        assertEquals(ErrorCode.CANNOT_DIVIDE_BY_ZERO, thrown.getErrorCode());
        verify(rpnRequest).expression();
    }

    @Test
    @DisplayName("Show hints Success Case")
    void showHints_Success() {
        MinesweeperRequest minesweeperRequest = mock(MinesweeperRequest.class);
        when(minesweeperRequest.square()).thenReturn(new String[]{"**...", ".....", ".*..."});
        String[] hints = challengeManager.showHints(minesweeperRequest).hints();
        assertArrayEquals(new String[]{"**100", "33200", "1*100"}, hints);
        verify(minesweeperRequest).square();
    }
}

