package com.pinsoft.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pinsoft.challenge.manager.ChallengeManager;
import com.pinsoft.challenge.model.request.MinesweeperRequest;
import com.pinsoft.challenge.model.request.RpnRequest;
import com.pinsoft.challenge.model.response.MinesweeperResponse;
import com.pinsoft.challenge.model.response.RpnResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {ChallengeController.class, ChallengeManager.class})
@WebMvcTest
class ChallengeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Calculate RPN expression")
    void calculate_Success() throws Exception {
        String expression = objectMapper.writeValueAsString(new RpnRequest("3 5 8 * 7 + *"));
        RpnResponse expectedResult = new RpnResponse(141);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/challenge/calculate").content(expression).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

        RpnResponse actualResult = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RpnResponse.class);
        assertEquals(expectedResult.result(), actualResult.result());
    }

    @Test
    void showHints_Success() throws Exception {
        String expression = objectMapper.writeValueAsString(new MinesweeperRequest(new String[]{"**...", ".....", ".*..."}));
        MinesweeperResponse expectedResult = new MinesweeperResponse(new String[]{"**100", "33200", "1*100"});
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/challenge/show-hints").content(expression).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

        MinesweeperResponse actualResult = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), MinesweeperResponse.class);
        assertArrayEquals(expectedResult.hints(), actualResult.hints());
    }
}

